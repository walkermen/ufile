<?php
namespace Yanan\Ufile\Ucloud;


class MakeAuth {

    function UCloud_MakeAuth($auth)
    {
        if (isset($auth)) {
            return $auth;
        }

        $UCLOUD_PUBLIC_KEY = config('ufile.UCLOUD_PUBLIC_KEY');
        $UCLOUD_PRIVATE_KEY = config('ufile.UCLOUD_PRIVATE_KEY');

        return new UcloudAuth($UCLOUD_PUBLIC_KEY, $UCLOUD_PRIVATE_KEY);
    }

}




