<?php
namespace Yanan\Ufile\Ucloud;

class CheckStatus
{
    const NO_AUTH_CHECK   = 0;
    const HEAD_FIELD_CHECK  = 1;
    const QUERY_STRING_CHECK     = 2;
}
