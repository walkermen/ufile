<?php
namespace Yanan\Ufile;

use Illuminate\Support\Facades\Log;
use Yanan\Ufile\Ucloud\Proxy;

class UpFile
{

    /**
     * ufile 文件下载.
     * @param  string $bucket
     * @param  string $key
     * @param  string $type
     * @return  array
     */
    public function getFile($bucket,$key,$type='public')
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            Log::info('存储名称不能为空'.$key);
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        $proxy = new Proxy();

        if ($type == 'public'){

            /*
             * 访问公有Bucket的例子
             */
            $url = $proxy->UCloud_MakePublicUrl($bucket, $key);

        }else{
            /*
             * 访问包含过期时间的私有Bucket例子
             */
            $curtime = time();
            $curtime += 60; // 有效期60秒

            $url = $proxy->UCloud_MakePrivateUrl($bucket, $key, $curtime);

        }

        $content = $this->curl_file_get_contents($url);

        return array(
            'code' => '200',
            'msg' => $content
        );

    }

    public function deleteFile($bucket, $key)
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        $proxy = new Proxy();

        list($data, $err) = $proxy->UCloud_Delete($bucket, $key);

        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

        return array(
            'code' => '200',
            'msg' => '删除成功！'
        );

    }

    //该接口适用于web的POST表单上传
    public function multipartFile($bucket, $key, $file)
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        //待上传文件的本地路径
        if ( empty($file) ){
            return array(
                'code' => '002',
                'error' => '本地文件路径不能为空'
            );
        }

        //该接口适用于web的POST表单上传,本SDK为了完整性故带上该接口demo.
        //服务端上传建议使用分片上传接口,而非POST表单
        $proxy = new Proxy();

        list($data, $err) = $proxy->UCloud_MultipartForm($bucket, $key, $file);

        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }
        return array(
            'code' => '200',
            'msg' => $data['ETag']
        );
    }

    public function muploadFile($bucket, $key, $file)
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        //待上传文件的本地路径
        if ( empty($file) ){
            return array(
                'code' => '002',
                'error' => '本地文件路径不能为空'
            );
        }

        //初始化分片上传,获取本地上传的uploadId和分片大小
        $proxy = new Proxy();

        list($data, $err) = $proxy->UCloud_MInit($bucket, $key);

        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

        $uploadId = $data['UploadId'];
        $blkSize  = $data['BlkSize'];

        //数据上传
        $proxy = new Proxy();

        list($etagList, $err) = $proxy->UCloud_MUpload($bucket, $key, $file, $uploadId, $blkSize);
        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

        //完成上传
        $proxy = new Proxy();

        list($data, $err) = $proxy->UCloud_MFinish($bucket, $key, $uploadId, $etagList);
        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

        return array(
            'code' => '200',
            'msg' => '上传成功！'."Etag:" . $data['ETag'] ."----"."FileSize: " . $data['FileSize']
        );

    }

    /**
     * ufile 文件上传.
     * @param  string $bucket
     * @param  string $key
     * @param  string $file
     * @return  array
     */
    public function putFile($bucket,$key,$file)
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        //待上传文件的本地路径
        if ( empty($file) ){
            return array(
                'code' => '002',
                'error' => '本地文件路径不能为空'
            );
        }

        $proxy = new Proxy();
        list($data, $err) = $proxy->UCloud_PutFile($bucket, $key, $file);
        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

        return array(
            'code' => '200',
            'msg' => $data['ETag']
        );
    }

    //秒传
    public function uploadhitFile($bucket, $key, $file)
    {
        //存储空间名
        $bucket = isset( $bucket ) ? $bucket : config('ufile.bucket');

        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        if ( empty($key) ){
            return array(
                'code' => '001',
                'error' => '存储名称不能为空'
            );
        }

        //待上传文件的本地路径
        if ( empty($file) ){
            return array(
                'code' => '002',
                'error' => '本地文件路径不能为空'
            );
        }

        //该接口不是上传接口.如果秒传返回非200错误码,意味着该文件在服务器不存在
        //需要继续调用其他上传接口完成上传操作

        $proxy = new Proxy();

        list($data, $err) = $proxy->UCloud_UploadHit($bucket, $key, $file);

        if ($err) {
            return array(
                'code' => $err->Code,
                'error' => $err->ErrMsg
            );
        }

    }

    public function curl_file_get_contents($durl){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $durl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ; // 获取数据返回
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true) ; // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回
        $r = curl_exec($ch);
        curl_close($ch);
        return $r;
    }





}
